module recharge

go 1.14

require (
	github.com/astaxie/beego v1.12.2
	github.com/dchest/captcha v0.0.0-20170622155422-6a29415a8364
	github.com/go-sql-driver/mysql v1.5.0
	github.com/shopspring/decimal v1.2.0
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/text v0.3.3
)

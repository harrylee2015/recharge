/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50640
Source Host           : localhost:3306
Source Database       : payfor

Target Server Type    : MYSQL
Target Server Version : 50640
File Encoding         : 65001

login_username		  : Joker  
login_password		  : Joker.

Date: 2019-11-14 14:55:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bank_info
-- ----------------------------
DROP TABLE IF EXISTS `bank_info`;
CREATE TABLE `bank_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '存放银行名称与id映射',
  `bank_id` varchar(86) DEFAULT NULL COMMENT '金融机构ID',
  `bank_name` varchar(345) DEFAULT NULL COMMENT '金融机构名称',
  `bank_short_name` varchar(345) DEFAULT NULL COMMENT '金融机构简称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=393 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank_info
-- ----------------------------
INSERT INTO `bank_info` VALUES ('1', 'CCCB', '长春农村商业银行', '');
INSERT INTO `bank_info` VALUES ('2', 'NBC', '宁波通商银行', '');
INSERT INTO `bank_info` VALUES ('3', 'KFBANK', '开封市商业银行', '');
INSERT INTO `bank_info` VALUES ('4', 'NYBANK', '南阳银行', '');
INSERT INTO `bank_info` VALUES ('5', 'JNYZB', '浙江景宁银座村镇银行', '');
INSERT INTO `bank_info` VALUES ('6', 'YBYZB', '重庆渝北银座村镇银行', '');
INSERT INTO `bank_info` VALUES ('7', 'QJZYB', '重庆黔江银座村镇银行', '');
INSERT INTO `bank_info` VALUES ('8', 'GXNS', '广西壮族自治区农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('9', 'KEB', '外换银行', '');
INSERT INTO `bank_info` VALUES ('10', 'IBK', '企业银行', '');
INSERT INTO `bank_info` VALUES ('11', 'GSBC', '甘肃银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('12', 'ZJNSYH', '江苏镇江农村商业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('13', 'DMVB', '天骄蒙银村镇银行', '');
INSERT INTO `bank_info` VALUES ('14', 'FJHAXH', '惠安县农村信用合作联社', '');
INSERT INTO `bank_info` VALUES ('15', 'DLMB', '大连旅顺口蒙银村镇银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('16', 'ZYB', '中原银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('17', 'GZNXHX', '贵州花溪农村商业银行股份有限公司', '贵州花溪农商行');
INSERT INTO `bank_info` VALUES ('18', 'RCBJQ', '酒泉农村商业银行股份有限公司', '酒泉农商行');
INSERT INTO `bank_info` VALUES ('19', 'CNRCB', '中国农商银行', '中国农商银行');
INSERT INTO `bank_info` VALUES ('20', 'BFCC', '佛山农商银行', '佛山农商银行');
INSERT INTO `bank_info` VALUES ('21', 'GZRCB', '广州农商银行', '广州农商银行');
INSERT INTO `bank_info` VALUES ('22', 'NRCB', '南昌农村商业银行股份有限公司', '南昌农商行');
INSERT INTO `bank_info` VALUES ('23', 'SHZNCB', '石河子国民村镇银行有限责任公司', '石河子国民村镇银行');
INSERT INTO `bank_info` VALUES ('24', 'CLRCB', '山西长治黎都农村商业银行股份有限公司', '山西长治黎都农村商业银行股份有限公司');
INSERT INTO `bank_info` VALUES ('25', 'YHRCB', '浙江杭州余杭农村商业银行股份有限公司', '杭州余杭农村商业银行');
INSERT INTO `bank_info` VALUES ('26', 'YNJDNSB', '云南景东农村商业银行股份有限公司', '云南景东农商行');
INSERT INTO `bank_info` VALUES ('27', 'HKNSBC', '海口农村商业银行股份有限公司', '海口农村商业银行股份有限公司');
INSERT INTO `bank_info` VALUES ('28', 'XWBC', '四川新网银行股份有限公司', '四川新网银行股份有限公司');
INSERT INTO `bank_info` VALUES ('29', 'JXBC', '江西银行股份有限公司', '江西银行股份有限公司');
INSERT INTO `bank_info` VALUES ('30', 'LRCB', '龙湾农村商业银行股份有限公司', '龙湾农商行');
INSERT INTO `bank_info` VALUES ('31', 'KSLCCB', '昆山鹿城村镇银行', '昆山鹿城村镇银行');
INSERT INTO `bank_info` VALUES ('32', 'LFRB', '大连金州联丰村镇银行股份有限公司', '大连金州联丰村镇银行股份有限公司');
INSERT INTO `bank_info` VALUES ('33', 'WX', '微信', '微信');
INSERT INTO `bank_info` VALUES ('34', 'ALIPAY', '支付宝', '支付宝');
INSERT INTO `bank_info` VALUES ('35', 'QQPAY', 'QQ钱包', 'QQ钱包');
INSERT INTO `bank_info` VALUES ('36', 'SHRB', '上海华瑞银行', '上海华瑞银行');
INSERT INTO `bank_info` VALUES ('37', 'BSCB', '深圳南山宝生村镇银行股份有限公司', '深圳南山宝生村镇银行股份有限公司');
INSERT INTO `bank_info` VALUES ('38', 'SBRCB', '内蒙古陕坝农村商业银行股份有限公司', '内蒙古陕坝农村商业银行股份有限公司');
INSERT INTO `bank_info` VALUES ('39', 'KLRCB', '贵州凯里农村商业银行股份有限公司', '贵州凯里农村商业银行股份有限公司');
INSERT INTO `bank_info` VALUES ('40', 'JDPAY', '京东', '京东');
INSERT INTO `bank_info` VALUES ('41', 'DYLSB', '东营莱商村镇银行', '东营莱商村镇银行');
INSERT INTO `bank_info` VALUES ('42', 'CSRCB', '长沙农村商业银行股份有限公司', '长沙农村商业银行');
INSERT INTO `bank_info` VALUES ('43', 'SHMB', '三河蒙银村镇银行股份有限公司', '三河蒙银村镇银行股份有限公司');
INSERT INTO `bank_info` VALUES ('44', 'JYRCB', '江苏仪征农村商业银行股份有限公司', '江苏仪征农村商业银行股份有限公司');
INSERT INTO `bank_info` VALUES ('45', 'CNHVB', '长春南关惠民村镇银行', '惠民村镇');
INSERT INTO `bank_info` VALUES ('46', 'GYCB', '江苏灌云农村商业银行股份有限公司', '灌云农村商业银行');
INSERT INTO `bank_info` VALUES ('47', 'XYCB', '江苏新沂农村商业银行股份有限公司', '江苏新沂农村商业银行股份有限公司');
INSERT INTO `bank_info` VALUES ('48', 'HRCB', '徐州淮海农村商业银行股份有限公司', '淮海农商银行');
INSERT INTO `bank_info` VALUES ('49', 'CDNB', '承德市郊区农村信用合作社联合社', '承德市郊区农村信用合作社联合社');
INSERT INTO `bank_info` VALUES ('50', 'ERCB', '鄂尔多斯农村商业银行', '');
INSERT INTO `bank_info` VALUES ('51', 'QRCB', '青岛农村商业银行', '');
INSERT INTO `bank_info` VALUES ('52', 'GXSLNCB', '广西上林国民村镇银行有限责任公司', '广西上林国民村镇银行');
INSERT INTO `bank_info` VALUES ('53', 'GXYHNCB', '广西银海国民村镇银行有限责任公司', '广西银海国民村镇银行');
INSERT INTO `bank_info` VALUES ('54', 'GXQNNCB', '广西钦州市钦南国民村镇银行有限责任公司', '广西钦南国民村镇银行');
INSERT INTO `bank_info` VALUES ('55', 'GXPBNCB', '广西浦北国民村镇银行有限责任公司', '广西浦北国民村镇银行');
INSERT INTO `bank_info` VALUES ('56', 'FCNCB', '防城港防城国民村镇银行有限责任公司', '防城国民村镇银行');
INSERT INTO `bank_info` VALUES ('57', 'DXNCB', '东兴国民村镇银行有限责任公司', '东兴国民村镇银行');
INSERT INTO `bank_info` VALUES ('58', 'XJLZNCB', '新疆绿洲国民村镇银行有限责任公司', '新疆绿洲国民村镇银行');
INSERT INTO `bank_info` VALUES ('59', 'KLMYJLNCB', '克拉玛依金龙国民村镇银行有限责任公司', '克拉玛依金龙国民村镇银行');
INSERT INTO `bank_info` VALUES ('60', 'HMHXNCB', '哈密红星国民村镇银行有限责任公司', '哈密红星国民村镇银行');
INSERT INTO `bank_info` VALUES ('61', 'CJNCB', '昌吉国民村镇银行有限责任公司', '昌吉国民村镇银行');
INSERT INTO `bank_info` VALUES ('62', 'YLNCB', '伊犁国民村镇银行有限责任公司', '伊犁国民村镇银行');
INSERT INTO `bank_info` VALUES ('63', 'KTNCB', '奎屯国民村镇银行有限责任公司', '奎屯国民村镇银行');
INSERT INTO `bank_info` VALUES ('64', 'SYRCB', '沈阳农村商业银行', '沈阳农商行');
INSERT INTO `bank_info` VALUES ('65', 'XMRCB', '厦门农村商业银行股份有限公司', '厦门农村商业银行股份有限公司');
INSERT INTO `bank_info` VALUES ('66', 'YFYNNX', '郁南县农村信用合作联社', '郁南县农村信用合作联社');
INSERT INTO `bank_info` VALUES ('67', 'HKBC', '海口联合农商银行', '');
INSERT INTO `bank_info` VALUES ('68', 'HBNNX', '河北省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('69', 'XSHCX', '象山县绿叶城市信用社', '');
INSERT INTO `bank_info` VALUES ('70', 'ABBC', 'Allied Bank', '');
INSERT INTO `bank_info` VALUES ('71', 'BBBC', 'Bangkok Bank Pcl', '');
INSERT INTO `bank_info` VALUES ('72', 'BCEL', 'BCEL', '');
INSERT INTO `bank_info` VALUES ('73', 'BCBC', 'BC卡公司', '');
INSERT INTO `bank_info` VALUES ('74', 'CSCBC', 'CSC', '');
INSERT INTO `bank_info` VALUES ('75', 'DFSBC', 'Discover Financial Services，I', '');
INSERT INTO `bank_info` VALUES ('76', 'RBOSC', 'Royal Bank Open Stock Company', '');
INSERT INTO `bank_info` VALUES ('77', 'TRABC', 'Travelex', '');
INSERT INTO `bank_info` VALUES ('78', 'AHXHBC', '安徽当涂新华村镇银行', '');
INSERT INTO `bank_info` VALUES ('79', 'AHNX', '安徽省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('80', 'ASSB', '安顺市商业银行', '');
INSERT INTO `bank_info` VALUES ('81', 'HXBC', '广东华兴银行', '广东华兴银行');
INSERT INTO `bank_info` VALUES ('82', 'GZNHSB', '广东南海农村商业银行', '');
INSERT INTO `bank_info` VALUES ('83', 'NEBC', '广东南粤银行', '');
INSERT INTO `bank_info` VALUES ('84', 'GDNX', '广东省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('85', 'GDB', '广发银行', '');
INSERT INTO `bank_info` VALUES ('86', 'GXBB', '广西北部湾银行', '');
INSERT INTO `bank_info` VALUES ('87', 'GXNX', '广西农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('88', 'GZPYBC', '广州番禹新华村镇银行', '');
INSERT INTO `bank_info` VALUES ('89', 'GZSB', '广州农村商业银行', '');
INSERT INTO `bank_info` VALUES ('90', 'GZBC', '广州银行', '');
INSERT INTO `bank_info` VALUES ('91', 'GYBC', '贵阳银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('92', 'GZNX', '贵州省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('93', 'GLBC', '桂林银行', '');
INSERT INTO `bank_info` VALUES ('94', 'HRBBC', '哈尔滨银行', '');
INSERT INTO `bank_info` VALUES ('95', 'HSKBC', '哈萨克斯坦国民储蓄银行', '');
INSERT INTO `bank_info` VALUES ('96', 'HKSNBC', '海口苏南村镇银行', '');
INSERT INTO `bank_info` VALUES ('97', 'HANNX', '海南省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('98', 'HDSB', '邯郸银行', '');
INSERT INTO `bank_info` VALUES ('99', 'KBBC', '韩国KB', '');
INSERT INTO `bank_info` VALUES ('100', 'HGLT', '韩国乐天', '');
INSERT INTO `bank_info` VALUES ('101', 'HYBC', '韩亚银行（中国）', '');
INSERT INTO `bank_info` VALUES ('102', 'WHBC', '汉口银行', '');
INSERT INTO `bank_info` VALUES ('103', 'HZBC', '杭州银行', '杭州银行');
INSERT INTO `bank_info` VALUES ('104', 'HBBC', '河北银行', '');
INSERT INTO `bank_info` VALUES ('105', 'HENNX', '河南省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('106', 'HBIBC', '鹤壁银行', '');
INSERT INTO `bank_info` VALUES ('107', 'HLJNX', '黑龙江省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('108', 'EBCL', '恒丰银行', '');
INSERT INTO `bank_info` VALUES ('109', 'HSSH', '恒生银行', '');
INSERT INTO `bank_info` VALUES ('110', 'HSSSB', '衡水银行', '');
INSERT INTO `bank_info` VALUES ('111', 'HLSB', '葫芦岛银行', '');
INSERT INTO `bank_info` VALUES ('112', 'HBJBC', '湖北嘉鱼吴江村镇银行', '');
INSERT INTO `bank_info` VALUES ('113', 'HBNX', '湖北省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('114', 'HBSZBC', '湖北随州曾都汇丰村镇银行', '');
INSERT INTO `bank_info` VALUES ('115', 'XTNSB', '湖北仙桃北农商村镇银行', '');
INSERT INTO `bank_info` VALUES ('116', 'HUBBC', '湖北银行', '');
INSERT INTO `bank_info` VALUES ('117', 'HNNX', '湖南省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('118', 'CMB', '招商银行股份有限公司', '招商银行');
INSERT INTO `bank_info` VALUES ('119', 'ZJCZSB', '浙江稠州商业银行', '');
INSERT INTO `bank_info` VALUES ('120', 'ZJMTSB', '浙江民泰商业银行', '');
INSERT INTO `bank_info` VALUES ('121', 'ZJPHBC', '浙江平湖工银村镇银行', '');
INSERT INTO `bank_info` VALUES ('122', 'ZJNX', '浙江省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('123', 'ZJTLBC', '浙江泰隆商业银行', '');
INSERT INTO `bank_info` VALUES ('124', 'ZJCXBC', '浙江长兴联合村镇银行', '');
INSERT INTO `bank_info` VALUES ('125', 'ZSBC', '浙商银行', '');
INSERT INTO `bank_info` VALUES ('126', 'ZJSB', '镇江市商业银行', '');
INSERT INTO `bank_info` VALUES ('127', 'ZZBC', '郑州银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('128', 'ZGYS', '中国银盛', '');
INSERT INTO `bank_info` VALUES ('129', 'PSBC', '中国邮政储蓄银行', '');
INSERT INTO `bank_info` VALUES ('130', 'ZDXLBC', '中山小榄村镇银行', '');
INSERT INTO `bank_info` VALUES ('131', 'BOC', '中国银行', '');
INSERT INTO `bank_info` VALUES ('132', 'YTCBC', '中银通支付', '');
INSERT INTO `bank_info` VALUES ('133', 'CNCB', '中信银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('134', 'CQBSBC', '重庆璧山工银村镇银行', '');
INSERT INTO `bank_info` VALUES ('135', 'CQDZ', '重庆大足汇丰村镇银行', '');
INSERT INTO `bank_info` VALUES ('136', 'CQSB', '重庆农村商业银行', '');
INSERT INTO `bank_info` VALUES ('137', 'SXBC', '重庆三峡银行', '');
INSERT INTO `bank_info` VALUES ('138', 'CQBC', '重庆银行', '');
INSERT INTO `bank_info` VALUES ('139', 'ZKSB', '周口银行', '');
INSERT INTO `bank_info` VALUES ('140', 'ZHHR', '珠海华润银行', '');
INSERT INTO `bank_info` VALUES ('141', 'ZHNX', '珠海农村信用合作社联社', '');
INSERT INTO `bank_info` VALUES ('142', 'ZMDSB', '驻马店银行', '');
INSERT INTO `bank_info` VALUES ('143', 'ZGSB', '自贡市商业银行', '');
INSERT INTO `bank_info` VALUES ('144', 'ZYSB', '遵义市商业银行', '');
INSERT INTO `bank_info` VALUES ('145', 'ICBC', '中国工商银行', '工商银行');
INSERT INTO `bank_info` VALUES ('146', 'CEB', '中国光大银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('147', 'CCB', '中国建设银行', '');
INSERT INTO `bank_info` VALUES ('148', 'ABC', '中国农业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('149', 'UPOP', '中国银联', '');
INSERT INTO `bank_info` VALUES ('150', 'SHSB', '上海商业银行', '');
INSERT INTO `bank_info` VALUES ('151', 'BOS', '上海银行', '');
INSERT INTO `bank_info` VALUES ('152', 'SYBC', '上饶银行', '上饶银行');
INSERT INTO `bank_info` VALUES ('153', 'SXSB', '绍兴银行', '');
INSERT INTO `bank_info` VALUES ('154', 'BABC', '深圳宝安融兴村镇银行', '');
INSERT INTO `bank_info` VALUES ('155', 'SDB', '深圳发展银行', '');
INSERT INTO `bank_info` VALUES ('156', 'FTCBC', '深圳福田银座村镇银行', '');
INSERT INTO `bank_info` VALUES ('157', 'SZLGBC', '深圳龙岗鼎业村镇银行', '');
INSERT INTO `bank_info` VALUES ('158', 'SZNX', '深圳农村商业银行', '');
INSERT INTO `bank_info` VALUES ('159', 'PZHSB', '攀枝花市商业银行', '');
INSERT INTO `bank_info` VALUES ('160', 'PJSB', '盘锦市商业银行', '');
INSERT INTO `bank_info` VALUES ('161', 'PAB', '平安银行', '');
INSERT INTO `bank_info` VALUES ('162', 'PDSBC', '平顶山银行', '');
INSERT INTO `bank_info` VALUES ('163', 'PLSB', '平凉市商业银行', '');
INSERT INTO `bank_info` VALUES ('164', 'SPDB', '上海浦东发展银行', '');
INSERT INTO `bank_info` VALUES ('165', 'QLSB', '齐鲁银行', '');
INSERT INTO `bank_info` VALUES ('166', 'QSBC', '齐商银行', '');
INSERT INTO `bank_info` VALUES ('167', 'JCCBC', '蕲春中银富登村镇银行', '');
INSERT INTO `bank_info` VALUES ('168', 'QHDSB', '秦皇岛市商业银行', '');
INSERT INTO `bank_info` VALUES ('169', 'QDJNSB', '青岛即墨北农商村镇银行', '');
INSERT INTO `bank_info` VALUES ('170', 'QDBC', '青岛银行', '');
INSERT INTO `bank_info` VALUES ('171', 'QHNX', '青海省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('172', 'QHBC', '青海银行', '');
INSERT INTO `bank_info` VALUES ('173', 'QJSB', '曲靖市商业银行', '');
INSERT INTO `bank_info` VALUES ('174', 'QZSB', '泉州银行', '');
INSERT INTO `bank_info` VALUES ('175', 'RSJBC', '日本三菱信用卡公司', '');
INSERT INTO `bank_info` VALUES ('176', 'RZBC', '日照银行', '');
INSERT INTO `bank_info` VALUES ('177', 'SMXBC', '三门峡银行', '');
INSERT INTO `bank_info` VALUES ('178', 'XMBC', '厦门银行', '');
INSERT INTO `bank_info` VALUES ('179', 'SDNX', '山东省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('180', 'XZNX', '徐州市郊农村信用合作联社', '');
INSERT INTO `bank_info` VALUES ('181', 'XZSB', '徐州市商业银行', '');
INSERT INTO `bank_info` VALUES ('182', 'XCSB', '许昌市商业银行', '');
INSERT INTO `bank_info` VALUES ('183', 'XHCBC', '宣汉诚民村镇银行', '');
INSERT INTO `bank_info` VALUES ('184', 'YASB', '雅安市商业银行', '');
INSERT INTO `bank_info` VALUES ('185', 'YTCB', '烟台银行', '');
INSERT INTO `bank_info` VALUES ('186', 'YCBC', '盐城市商业银行', '');
INSERT INTO `bank_info` VALUES ('187', 'YQSB', '阳泉市商业银行', '');
INSERT INTO `bank_info` VALUES ('188', 'YDNX', '尧都区农村信用合作社联社', '');
INSERT INTO `bank_info` VALUES ('189', 'QSZY', '沂水中银富登村镇银行', '');
INSERT INTO `bank_info` VALUES ('190', 'YBSB', '宜宾市商业银行', '');
INSERT INTO `bank_info` VALUES ('191', 'YCSB', '宜昌市商业银行', '');
INSERT INTO `bank_info` VALUES ('192', 'YKSB', '营口银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('193', 'YLBC', '永隆银行', '');
INSERT INTO `bank_info` VALUES ('194', 'WBBJ', '友利银行', '');
INSERT INTO `bank_info` VALUES ('195', 'YXSB', '玉溪市商业银行', '');
INSERT INTO `bank_info` VALUES ('196', 'YNXG', '越南西贡商业银行', '');
INSERT INTO `bank_info` VALUES ('197', 'YNNX', '云南省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('198', 'SCCN', '渣打银行', '');
INSERT INTO `bank_info` VALUES ('199', 'ZJGNSB', '张家港农村商业银行', '');
INSERT INTO `bank_info` VALUES ('200', 'ZHKBC', '张家口市商业银行', '');
INSERT INTO `bank_info` VALUES ('201', 'CABC', '长安银行', '');
INSERT INTO `bank_info` VALUES ('202', 'CCSB', '长春市商业银行', '');
INSERT INTO `bank_info` VALUES ('203', 'CSBC', '长沙银行', '');
INSERT INTO `bank_info` VALUES ('204', 'CZHBC', '长治银行', '');
INSERT INTO `bank_info` VALUES ('205', 'SXNX', '山西省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('206', 'SHXNX', '陕西省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('207', 'SQBC', '商丘市商业银行', '');
INSERT INTO `bank_info` VALUES ('208', 'SRCB', '上海农村商业银行', '');
INSERT INTO `bank_info` VALUES ('209', 'SHNX', '上海农信', '');
INSERT INTO `bank_info` VALUES ('210', 'JMNSB', '江门新会农村商业银行', '');
INSERT INTO `bank_info` VALUES ('211', 'JNNSB', '江苏江南农村商业银行股份有限公司', '江南农村商业银行');
INSERT INTO `bank_info` VALUES ('212', 'JSMTBC', '江苏邗江民泰村镇银行', '');
INSERT INTO `bank_info` VALUES ('213', 'JSNX', '江苏省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('214', 'JSSYBC', '江苏沭阳东吴村镇银行', '');
INSERT INTO `bank_info` VALUES ('215', 'JSNSB', '江苏锡州农村商业银行', '');
INSERT INTO `bank_info` VALUES ('216', 'JSBC', '江苏银行', '');
INSERT INTO `bank_info` VALUES ('217', 'CZSB', '江苏长江商业银行', '');
INSERT INTO `bank_info` VALUES ('218', 'ZXGZBC', '江西赣州银座村镇银行', '');
INSERT INTO `bank_info` VALUES ('219', 'JXNX', '江西省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('220', 'JYNSB', '江阴市农村商业银行', '');
INSERT INTO `bank_info` VALUES ('221', 'BOCOM', '交通银行', '');
INSERT INTO `bank_info` VALUES ('222', 'JZSB', '焦作市商业银行', '');
INSERT INTO `bank_info` VALUES ('223', 'JHBC', '金华银行', '');
INSERT INTO `bank_info` VALUES ('224', 'JZBC', '锦州银行', '');
INSERT INTO `bank_info` VALUES ('225', 'JCBC', '晋城银行', '');
INSERT INTO `bank_info` VALUES ('226', 'JSHBC', '晋商银行', '');
INSERT INTO `bank_info` VALUES ('227', 'JZSSB', '晋中银行', '');
INSERT INTO `bank_info` VALUES ('228', 'JSZY', '京山中银富登村镇银行', '');
INSERT INTO `bank_info` VALUES ('229', 'JDZB', '景德镇市商业银行', '');
INSERT INTO `bank_info` VALUES ('230', 'JJBC', '九江银行', '');
INSERT INTO `bank_info` VALUES ('231', 'KHBC', '可汗银行', '');
INSERT INTO `bank_info` VALUES ('232', 'KELSB', '库尔勒市商业银行', '');
INSERT INTO `bank_info` VALUES ('233', 'KLBC', '昆仑银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('234', 'CMBC', '中国民生银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('235', 'MSK', '莫斯科人民储蓄银行', '');
INSERT INTO `bank_info` VALUES ('236', 'NCBC', '南昌银行', '');
INSERT INTO `bank_info` VALUES ('237', 'NQSB', '南充市商业银行', '');
INSERT INTO `bank_info` VALUES ('238', 'NJBC', '南京银行', '');
INSERT INTO `bank_info` VALUES ('239', 'NNSB', '南宁市商业银行', '');
INSERT INTO `bank_info` VALUES ('240', 'NTSB', '南通市商业银行', '');
INSERT INTO `bank_info` VALUES ('241', 'NYCBC', '南阳村镇银行', '');
INSERT INTO `bank_info` VALUES ('242', 'NYSBC', '南洋商业银行', '');
INSERT INTO `bank_info` VALUES ('243', 'MGBC', '内蒙古银行', '');
INSERT INTO `bank_info` VALUES ('244', 'NMNX', '内蒙古自治区农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('245', 'NBDHBC', '宁波东海银行', '');
INSERT INTO `bank_info` VALUES ('246', 'NBBC', '宁波银行', '');
INSERT INTO `bank_info` VALUES ('247', 'NBYZBC', '宁波鄞州农村合作银行', '');
INSERT INTO `bank_info` VALUES ('248', 'NXBC', '宁夏银行', '');
INSERT INTO `bank_info` VALUES ('249', 'HUZBC', '湖州银行', '');
INSERT INTO `bank_info` VALUES ('250', 'CTSH', '花旗银行', '');
INSERT INTO `bank_info` VALUES ('251', 'HQBC', '华侨银行（中国）', '');
INSERT INTO `bank_info` VALUES ('252', 'HRXJBC', '华融湘江银行', '');
INSERT INTO `bank_info` VALUES ('253', 'HXB', '华夏银行', '');
INSERT INTO `bank_info` VALUES ('254', 'HHNSB', '黄河农村商业银行', '');
INSERT INTO `bank_info` VALUES ('255', 'KMNL', '昆明市农村信用合作社联合社', '');
INSERT INTO `bank_info` VALUES ('256', 'KMSB', '昆明市商业银行', '');
INSERT INTO `bank_info` VALUES ('257', 'KLNX', '昆山市农村信用合作社联合社', '');
INSERT INTO `bank_info` VALUES ('258', 'LSBC', '莱商银行', '');
INSERT INTO `bank_info` VALUES ('259', 'LWBC', '莱芜银行', '');
INSERT INTO `bank_info` VALUES ('260', 'LZBC', '兰州银行', '');
INSERT INTO `bank_info` VALUES ('261', 'LFBC', '廊坊银行', '');
INSERT INTO `bank_info` VALUES ('262', 'LSSB', '乐山市商业银行', '');
INSERT INTO `bank_info` VALUES ('263', 'LYGSB', '连云港市商业银行', '');
INSERT INTO `bank_info` VALUES ('264', 'LSZSB', '凉山州商业银行', '');
INSERT INTO `bank_info` VALUES ('265', 'LNNX', '辽宁省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('266', 'LYGBC', '辽阳银行', '');
INSERT INTO `bank_info` VALUES ('267', 'LFYDNX', '临汾市尧都区农村信用合作联社', '');
INSERT INTO `bank_info` VALUES ('268', 'LFSBC', '临商银行', '');
INSERT INTO `bank_info` VALUES ('269', 'LYCB', '临沂商业银行', '');
INSERT INTO `bank_info` VALUES ('270', 'LZHBC', '柳州银行', '');
INSERT INTO `bank_info` VALUES ('271', 'LPBC', '六盘水市商业银行', '');
INSERT INTO `bank_info` VALUES ('272', 'LJBC', '龙江银行', '');
INSERT INTO `bank_info` VALUES ('273', 'LZSB', '泸州市商业银行', '');
INSERT INTO `bank_info` VALUES ('274', 'LYBC', '洛阳银行', '');
INSERT INTO `bank_info` VALUES ('275', 'LHSB', '漯河市商业银行', '');
INSERT INTO `bank_info` VALUES ('276', 'MGGLM', '蒙古郭勒姆特银行', '');
INSERT INTO `bank_info` VALUES ('277', 'JYSB', '绵阳市商业银行', '');
INSERT INTO `bank_info` VALUES ('278', 'SJBC', '盛京银行', '');
INSERT INTO `bank_info` VALUES ('279', 'SZSBC', '石嘴山银行', '');
INSERT INTO `bank_info` VALUES ('280', 'SLCBC', '双流诚民村镇银行', '');
INSERT INTO `bank_info` VALUES ('281', 'SCBC', '丝绸之路银行', '');
INSERT INTO `bank_info` VALUES ('282', 'SCNX', '四川省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('283', 'SZSB', '苏州市商业银行', '');
INSERT INTO `bank_info` VALUES ('284', 'SZBC', '苏州银行', '');
INSERT INTO `bank_info` VALUES ('285', 'SNSB', '遂宁市商业银行', '');
INSERT INTO `bank_info` VALUES ('286', 'TZBC', '台州银行', '');
INSERT INTO `bank_info` VALUES ('287', 'TCNSB', '太仓农村商业银行', '');
INSERT INTO `bank_info` VALUES ('288', 'TASB', '泰安市商业银行', '');
INSERT INTO `bank_info` VALUES ('289', 'TLXS', '泰隆城市信用社', '');
INSERT INTO `bank_info` VALUES ('290', 'TSBC', '唐山市商业银行', '');
INSERT INTO `bank_info` VALUES ('291', 'TJBHNX', '天津滨海农村商业银行', '');
INSERT INTO `bank_info` VALUES ('292', 'TRCB', '天津农商银行', '');
INSERT INTO `bank_info` VALUES ('293', 'TJBC', '天津银行', '');
INSERT INTO `bank_info` VALUES ('294', 'TLBC', '铁岭银行', '');
INSERT INTO `bank_info` VALUES ('295', 'WHSB', '威海市商业银行', '');
INSERT INTO `bank_info` VALUES ('296', 'WFCB', '潍坊银行', '');
INSERT INTO `bank_info` VALUES ('297', 'WZBC', '温州银行', '');
INSERT INTO `bank_info` VALUES ('298', 'WHGBC', '乌海银行', '');
INSERT INTO `bank_info` VALUES ('299', 'WSBC', '乌鲁木齐市商业银行', '');
INSERT INTO `bank_info` VALUES ('300', 'WXSB', '无锡市商业银行', '');
INSERT INTO `bank_info` VALUES ('301', 'WJNSB', '吴江农村商业银行', '');
INSERT INTO `bank_info` VALUES ('302', 'WHNSB', '武汉农村商业银行', '');
INSERT INTO `bank_info` VALUES ('303', 'XABC', '西安银行', '');
INSERT INTO `bank_info` VALUES ('304', 'XFNSB', '咸丰常农商村镇银行', '');
INSERT INTO `bank_info` VALUES ('305', 'XHBC', '新韩银行', '');
INSERT INTO `bank_info` VALUES ('306', 'XJPB', '新加坡大华银行', '');
INSERT INTO `bank_info` VALUES ('307', 'SXPB', '新加坡星网电子付款私人有限公司', '');
INSERT INTO `bank_info` VALUES ('308', 'XJHHBC', '新疆汇和银行', '');
INSERT INTO `bank_info` VALUES ('309', 'XJNX', '新疆维吾尔自治区农村信用社联合社', '新疆维吾尔自治区农村信用社联合社');
INSERT INTO `bank_info` VALUES ('310', 'XXSB', '新乡银行', '');
INSERT INTO `bank_info` VALUES ('311', 'XYBC', '信阳银行', '');
INSERT INTO `bank_info` VALUES ('312', 'DBSC', '星展银行香港有限公司', '');
INSERT INTO `bank_info` VALUES ('313', 'XTBC', '邢台银行', '');
INSERT INTO `bank_info` VALUES ('314', 'CIB', '兴业银行', '');
INSERT INTO `bank_info` VALUES ('315', 'ASBC', '鞍山银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('316', 'ADFB', '澳门大丰银行', '');
INSERT INTO `bank_info` VALUES ('317', 'AGB', '澳门商业银行', '');
INSERT INTO `bank_info` VALUES ('318', 'ATYC', '澳门通股份有限公司', '');
INSERT INTO `bank_info` VALUES ('319', 'AYBC', '安阳银行', '');
INSERT INTO `bank_info` VALUES ('320', 'FSBB', '巴基斯坦FAYSAL BANK', '');
INSERT INTO `bank_info` VALUES ('321', 'BSBC', '包商银行', '');
INSERT INTO `bank_info` VALUES ('322', 'BJSB', '宝鸡市商业银行', '');
INSERT INTO `bank_info` VALUES ('323', 'BDSB', '保定银行', '');
INSERT INTO `bank_info` VALUES ('324', 'HRCBC', '北京怀柔融兴村镇银行', '');
INSERT INTO `bank_info` VALUES ('325', 'MYFFBC', '北京密云汇丰村镇银行', '');
INSERT INTO `bank_info` VALUES ('326', 'BRCB', '北京农村商业银行', '');
INSERT INTO `bank_info` VALUES ('327', 'SYCBC', '北京顺义银座村镇银行', '');
INSERT INTO `bank_info` VALUES ('328', 'BCCB', '北京银行', '');
INSERT INTO `bank_info` VALUES ('329', 'BOHC', '渤海银行', '');
INSERT INTO `bank_info` VALUES ('330', 'CZBC', '沧州银行', '');
INSERT INTO `bank_info` VALUES ('331', 'CSSB', '常熟市农村商业银行', '');
INSERT INTO `bank_info` VALUES ('332', 'CYBC', '朝阳银行', '');
INSERT INTO `bank_info` VALUES ('333', 'CDSBC', '成都银行', '');
INSERT INTO `bank_info` VALUES ('334', 'CDBC', '承德银行', '');
INSERT INTO `bank_info` VALUES ('335', 'CXBC', '创兴银行', '');
INSERT INTO `bank_info` VALUES ('336', 'DZSB', '达州市商业银行', '');
INSERT INTO `bank_info` VALUES ('337', 'DFBC', '大丰银行', '');
INSERT INTO `bank_info` VALUES ('338', 'DHBC', '大华银行（中国）', '');
INSERT INTO `bank_info` VALUES ('339', 'DLBC', '大连银行', '');
INSERT INTO `bank_info` VALUES ('340', 'DTSB', '大同银行', '');
INSERT INTO `bank_info` VALUES ('341', 'AXBC', '大西洋银行', '');
INSERT INTO `bank_info` VALUES ('342', 'DXBC', '大新银行', '');
INSERT INTO `bank_info` VALUES ('343', 'DYCBC', '大邑交银兴民村镇银行', '');
INSERT INTO `bank_info` VALUES ('344', 'DDBC', '丹东银行', '');
INSERT INTO `bank_info` VALUES ('345', 'DYBC', '德阳银行', '');
INSERT INTO `bank_info` VALUES ('346', 'DZBC', '德州银行', '');
INSERT INTO `bank_info` VALUES ('347', 'DGNSB', '东莞农村商业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('348', 'BEAI', '东亚银行', '');
INSERT INTO `bank_info` VALUES ('349', 'DOYBC', '俄罗斯远东商业银行', '');
INSERT INTO `bank_info` VALUES ('350', 'ERDSBC', '鄂尔多斯银行', '');
INSERT INTO `bank_info` VALUES ('351', 'ESNSB', '恩施常农商村镇银行', '');
INSERT INTO `bank_info` VALUES ('352', 'FCIB', '法国兴业银行（中国）', '');
INSERT INTO `bank_info` VALUES ('353', 'FDCBC', '方大村镇银行', '');
INSERT INTO `bank_info` VALUES ('354', 'BDOB', '菲律宾BDO', '');
INSERT INTO `bank_info` VALUES ('355', 'RCBC', '菲律宾RCBC', '');
INSERT INTO `bank_info` VALUES ('356', 'FSSSNX', '佛山市三水区农村信用合作社', '');
INSERT INTO `bank_info` VALUES ('357', 'FSSDBC', '佛山顺德农村商业银行', '');
INSERT INTO `bank_info` VALUES ('358', 'FHBC', '福建海峡银行', '');
INSERT INTO `bank_info` VALUES ('359', 'FJSCBC', '福建建瓯石狮村镇银行', '');
INSERT INTO `bank_info` VALUES ('360', 'FJNX', '福建省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('361', 'FSBC', '抚顺银行', '');
INSERT INTO `bank_info` VALUES ('362', 'FXBC', '阜新银行', '');
INSERT INTO `bank_info` VALUES ('363', 'FDB', '富滇银行', '');
INSERT INTO `bank_info` VALUES ('364', 'GSNX', '甘肃省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('365', 'GZHBC', '赣州银行', '');
INSERT INTO `bank_info` VALUES ('366', 'HSBC', '徽商银行', '徽商银行');
INSERT INTO `bank_info` VALUES ('367', 'HKSH', '汇丰银行(中国)', '');
INSERT INTO `bank_info` VALUES ('368', 'JLNX', '吉林省农村信用社联合社', '');
INSERT INTO `bank_info` VALUES ('369', 'JLSB', '吉林银行', '');
INSERT INTO `bank_info` VALUES ('370', 'JYBC', '集友银行', '');
INSERT INTO `bank_info` VALUES ('371', 'JNBC', '济宁银行', '');
INSERT INTO `bank_info` VALUES ('372', 'JXSB', '嘉兴银行', '');
INSERT INTO `bank_info` VALUES ('373', 'AEONBC', 'AEON信贷财务亚洲有限公司', '');
INSERT INTO `bank_info` VALUES ('374', 'HZUB', '杭州联合农村商业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('375', 'HZBANK', '杭州银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('376', 'DGBC', '东莞银行', '');
INSERT INTO `bank_info` VALUES ('377', 'NXY', '农信银支付清算系统', '');
INSERT INTO `bank_info` VALUES ('378', 'BGZC', '贵州银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('379', 'ADBC', '中国农业发展银行', '');
INSERT INTO `bank_info` VALUES ('380', 'FSBANK', '华一银行', '');
INSERT INTO `bank_info` VALUES ('381', 'RBS', '苏格兰皇家银行', '');
INSERT INTO `bank_info` VALUES ('382', 'EXIM', '中国进出口银行', '');
INSERT INTO `bank_info` VALUES ('383', 'CDB', '国家开发银行', '');
INSERT INTO `bank_info` VALUES ('384', 'DYCCB', '东营市商业银行', '');
INSERT INTO `bank_info` VALUES ('385', 'SMYZB', '浙江三门银座村镇银行', '');
INSERT INTO `bank_info` VALUES ('386', 'ZRCB', '珠海农村商业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('387', 'COASTALB', '营口沿海银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('388', 'QNBANK', '陕西秦农农村商业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('389', 'XMINTBANK', '厦门国际银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('390', 'JTNSH', '吉林九台农村商业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('391', 'JSRCB', '浙江江山农村商业银行股份有限公司', '');
INSERT INTO `bank_info` VALUES ('392', 'GRCB', '贵阳农村商业银行', '');

-- ----------------------------
-- Table structure for custom_plus_minus
-- ----------------------------
DROP TABLE IF EXISTS `custom_plus_minus`;
CREATE TABLE `custom_plus_minus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL,
  `version` int(8) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `user_name` varchar(58) DEFAULT NULL,
  `addition` decimal(20,4) DEFAULT NULL COMMENT '加款',
  `deduction` decimal(20,4) DEFAULT NULL COMMENT '减款',
  `handing_fee` decimal(20,4) DEFAULT NULL COMMENT '手续费（包含费率）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定义加减款';

-- ----------------------------
-- Records of custom_plus_minus
-- ----------------------------

-- ----------------------------
-- Table structure for merchant
-- ----------------------------
DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `version` int(11) NOT NULL DEFAULT '0',
  `merchant_name` varchar(65) NOT NULL COMMENT '商户名',
  `merchant_no` varchar(45) NOT NULL COMMENT '商户编号',
  `secret_key` text NOT NULL COMMENT '密钥',
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL,
  `total_amount` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '总金额',
  `usable_amount` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '可用金额',
  `frozen_amount` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '冻结金额',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '状态,0:可见,1:不可见',
  `channel_type` varchar(10) NOT NULL COMMENT '通道类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `merchant_no_UNIQUE` (`merchant_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of merchant
-- ----------------------------
INSERT INTO `merchant` VALUES ('44', '0', '69', 'M243047573', 'M243047573', 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsLEC28DJvFAlKFVxZNmV2xA40jBWFaePtSgFLOYHY7PVSziuImR+TmaI+7zDNIQGeR7f12JGlguH0H67lKorMZwDhixY0y6p/shGzbHb+Ifo8rX2Q8xkRDqEPUw5RpgKc1p1CaBgmfnsVgkWu9poiIlTSnTifwKZPX6Zg614GWbfAE0DbNGivC4CAFF8+1QwouYrRtgxqG6K4N1FPc1Asv+LfGlThudn48QDJGFWp/+IJh/jtis/RLB33434QAB', '2019-04-08 11:35:00', '2019-06-04 10:47:40', '7050740.2100', '7050740.2100', '0.0000', '1', 'XF');
INSERT INTO `merchant` VALUES ('45', '0', '2', '青岛瓦丝', 'M203442155', 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkC4kZYxADfRFqc+Nn4q5VTtD57dCUTIYMZorLlok1fJjwlamxsuWD9dX5pyUw3KcZdXekGBNsA5wZGrzW4ZMTrrV+LiokdCRD8CBCTLPGQ6DDKveaL3aObAvFFmJzQKFphJTZ2XOh0J/4ImwRMyX04MJk6NRk/HSS4aqo6Enw7RAqU84CzOWLEFR135WHLWn7Fx9ISwW9tHWrecvOma5b/Scmn4QPHAebRxzJX/7E+dVQTiszjChc3IBuS4Ws0tfMN1I3N+90C7Wo24UdK0sjr/gcvU4VtprpmK2xCzaAq3bvxVqdcpnzOIuFt0N34434QAB', '2019-06-04 10:43:29', '2019-06-04 12:24:04', '0.0000', '0.0000', '0.0000', '0', 'XF');

-- ----------------------------
-- Table structure for recharge_record
-- ----------------------------
DROP TABLE IF EXISTS `recharge_record`;
CREATE TABLE `recharge_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户Id',
  `merchant_no` varchar(45) NOT NULL COMMENT '商户编号',
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL,
  `serial_number` varchar(48) NOT NULL COMMENT '流水号',
  `re_order_id` varchar(48) NOT NULL COMMENT '充值订单号',
  `re_amount` decimal(20,4) NOT NULL COMMENT '充值金额',
  `re_account_no` varchar(65) NOT NULL COMMENT '充值银行卡号',
  `re_account_name` varchar(125) NOT NULL COMMENT '银行账号对应的户名',
  `re_certificate_type` int(2) DEFAULT NULL COMMENT '证件类型：0(身份证)',
  `re_certificate_no` varchar(65) DEFAULT NULL COMMENT '证件编号',
  `re_mobile_no` varchar(18) DEFAULT NULL COMMENT '手机号',
  `re_recevie_bank` varchar(10) NOT NULL COMMENT '备付金账户标识',
  `status` varchar(36) NOT NULL COMMENT '充值状态',
  `remark` varchar(225) DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL DEFAULT '0',
  `record_type` varchar(10) DEFAULT NULL,
  `notice_url` text COMMENT '异步通知地址',
  `record_class` varchar(10) DEFAULT NULL COMMENT '订单类别,区分是否是对接产生的订单',
  `api_notice_url` text COMMENT '对接用户的异步通知地址',
  `item` varchar(125) DEFAULT NULL COMMENT '对接，扩展字段',
  PRIMARY KEY (`id`),
  KEY `xid_order_id` (`re_order_id`) USING BTREE,
  KEY `xid_user_id` (`user_id`) USING BTREE,
  KEY `x_edit_time` (`edit_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recharge_record
-- ----------------------------

-- ----------------------------
-- Table structure for transfer_record
-- ----------------------------
DROP TABLE IF EXISTS `transfer_record`;
CREATE TABLE `transfer_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '收银台转账记录表',
  `user_id` int(11) NOT NULL,
  `merchant_no` varchar(45) NOT NULL COMMENT '商户编号',
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL,
  `serial_number` varchar(48) NOT NULL COMMENT '流水号',
  `tr_order_id` varchar(48) NOT NULL COMMENT '商户订单号',
  `tr_amount` decimal(20,4) NOT NULL COMMENT '金额 ',
  `tr_account_type` char(2) NOT NULL COMMENT '卡种',
  `tr_product_name` varchar(145) NOT NULL COMMENT '商品名称',
  `status` varchar(36) NOT NULL,
  `tr_notice_url` text,
  `remark` varchar(245) DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `xtr_order_id` (`tr_order_id`) USING BTREE,
  KEY `xid_user_id` (`user_id`) USING BTREE,
  KEY `x_edit_time` (`edit_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transfer_record
-- ----------------------------

-- ----------------------------
-- Table structure for user_history
-- ----------------------------
DROP TABLE IF EXISTS `user_history`;
CREATE TABLE `user_history` (
  `id` int(23) NOT NULL AUTO_INCREMENT COMMENT '用户加减款历史操作表',
  `version` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `user_name` varchar(58) DEFAULT NULL,
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL,
  `addition` decimal(20,4) DEFAULT NULL COMMENT '加',
  `deduction` decimal(20,4) DEFAULT NULL COMMENT '减款',
  `frozen_add` decimal(20,4) DEFAULT NULL COMMENT '冻结,冻结加,',
  `frozen_ded` decimal(20,4) DEFAULT NULL COMMENT '冻结减',
  `usable_amount` decimal(20,4) NOT NULL COMMENT '总可用',
  `total_amount` decimal(20,4) NOT NULL COMMENT '总',
  `frozen_amount` decimal(20,4) DEFAULT NULL COMMENT '总冻结',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_history
-- ----------------------------

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT '0' COMMENT '版本',
  `mobile` varchar(14) NOT NULL COMMENT '手机号',
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL COMMENT '修改时间',
  `user_name` varchar(45) NOT NULL COMMENT '用户名',
  `login_name` varchar(52) NOT NULL COMMENT '登录账号',
  `login_pwd` varchar(58) NOT NULL COMMENT '密码',
  `salt` varchar(65) NOT NULL COMMENT '密码加盐',
  `authority` int(2) NOT NULL DEFAULT '1' COMMENT '权限1:VIP用户;2:普通用户;3:对接用户',
  `total_amount` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '通道总金额',
  `usable_amount` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '可用金额',
  `frozen_amount` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '冻结金额',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '状态,0:可见,1:不可见',
  `api_key` text COMMENT '对接用户的api密钥',
  `pay_fee` decimal(20,4) NOT NULL DEFAULT '1.0000' COMMENT '代付单笔手续费,元',
  `recharge_fee` decimal(20,4) NOT NULL DEFAULT '10.0000' COMMENT '充值单笔手续费,元',
  `recharge_rate` decimal(20,4) NOT NULL DEFAULT '1.0000' COMMENT '充值单笔费率,千分之几',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_name_UNIQUE` (`login_name`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='用户信息';

-- ----------------------------
-- Records of user_info
# +---------------+--------------+------+-----+---------+-------+
# | Field         | Type         | Null | Key | Default | Extra |
# +---------------+--------------+------+-----+---------+-------+
# | id            | int          | NO   | PRI | NULL    |       |
# | version       | int          | NO   |     | 0       |       |
# | mobile        | varchar(255) | NO   |     |         |       |
# | status        | int          | NO   |     | 0       |       |
# | edit_time     | varchar(255) | NO   |     |         |       |
# | create_time   | varchar(255) | NO   |     |         |       |
# | user_name     | varchar(255) | NO   |     |         |       |
# | login_name    | varchar(255) | NO   |     |         |       |
# | login_pwd     | varchar(255) | NO   |     |         |       |
# | salt          | varchar(255) | NO   |     |         |       |
# | authority     | int          | NO   |     | 0       |       |
# | total_amount  | double       | NO   |     | 0       |       |
# | usable_amount | double       | NO   |     | 0       |       |
# | frozen_amount | double       | NO   |     | 0       |       |
# | api_key       | longtext     | NO   |     | NULL    |       |
# | pay_fee       | double       | NO   |     | 0       |       |
# | recharge_fee  | double       | NO   |     | 0       |       |
# | recharge_rate | double       | NO   |     | 0       |       |
# +---------------+--------------+------+-----+---------+-------+

-- ----------------------------
INSERT INTO `user_info` (id,version,mobile,create_time,edit_time,user_name,login_name,login_pwd,salt,authority,total_amount,usable_amount,frozen_amount,status,api_key,pay_fee,recharge_fee,recharge_rate)VALUES ('-1', '111', '18890001842', '2019-04-01 12:55:19', '2019-05-29 11:51:18', 'Joker', 'Joker', 'fd96f9b05bec6da6ae90073a3a0bdf70', 'Gb3QXA6WO0BERTMZzEjv0b1m8B1VmCEY', '-1', '1000.9210', '1000.9220', '12.0000', '0', '8O0XXnKCq3vsMiuqZK8XaaiqjlZ6wQ0m5Tir', '1.0000', '10.0000', '1.0000');

-- ----------------------------
-- Table structure for user_info_expansion
-- ----------------------------
DROP TABLE IF EXISTS `user_info_expansion`;
CREATE TABLE `user_info_expansion` (
  `user_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL,
  `ips` text COMMENT 'ip白名单',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息扩展';

-- ----------------------------
-- Records of user_info_expansion
-- ----------------------------

-- ----------------------------
-- Table structure for withdraw_record
-- ----------------------------
DROP TABLE IF EXISTS `withdraw_record`;
CREATE TABLE `withdraw_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `merchant_no` varchar(45) NOT NULL COMMENT '商户编号',
  `create_time` varchar(20) NOT NULL,
  `edit_time` varchar(20) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `serial_number` varchar(48) NOT NULL COMMENT '序列号',
  `wh_order_id` varchar(48) NOT NULL COMMENT '商户订单号',
  `wh_amount` decimal(20,4) NOT NULL COMMENT '金额',
  `wh_user_type` varchar(2) NOT NULL COMMENT '用户类型1：对私 2：对公',
  `wh_issuer` varchar(65) DEFAULT NULL COMMENT '联行号',
  `wh_account_no` varchar(65) NOT NULL COMMENT '卡号',
  `wh_account_name` varchar(125) NOT NULL COMMENT '持卡人姓名',
  `wh_account_type` varchar(10) DEFAULT NULL COMMENT '账户类型',
  `wh_mobile_no` varchar(18) DEFAULT NULL COMMENT '手机号',
  `wh_bank_no` varchar(30) NOT NULL COMMENT '银行编码',
  `wh_branch_province` varchar(45) DEFAULT NULL COMMENT '开户省',
  `wh_branch_city` varchar(45) DEFAULT NULL COMMENT '开户市 ',
  `wh_branch_name` varchar(45) DEFAULT NULL COMMENT '开户支行名称',
  `status` varchar(36) NOT NULL COMMENT '状态',
  `record_type` varchar(10) DEFAULT NULL,
  `remark` varchar(245) DEFAULT NULL COMMENT '备注',
  `notice_url` text COMMENT '异步通知地址',
  `record_class` varchar(10) DEFAULT NULL COMMENT '订单类别,区分是否是对接产生的订单',
  `api_notice_url` text,
  `item` varchar(125) DEFAULT NULL COMMENT '对接，扩展字段',
  PRIMARY KEY (`id`),
  KEY `xwh_order_id` (`wh_order_id`) USING BTREE,
  KEY `xid_user_id` (`user_id`) USING BTREE,
  KEY `x_edit_time` (`edit_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of withdraw_record
-- ----------------------------

/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019.04.02 9:54 
 ** @Author : Joker
 ** @File : merchant
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.02 9:54
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/sys"
	"recharge/utils"
)

type Merchant struct {
	Id           int
	UserId       int
	Version      int
	MerchantName string                    //商户名
	MerchantNo   string                    //商户编号
	SecretKey    string `orm:"type(text)"` //密钥
	CreateTime   string
	EditTime     string
	TotalAmount  float64 //总金额
	UsableAmount float64 //可用金额
	FrozenAmount float64 //冻结金额
	Status       int     //状态
	ChannelType  string  //通道类型
}

func (*Merchant) TableEngine() string {
	return "INNODB"
}

func (*Merchant) TableName() string {
	return MerchantTBName()
}

/* *
 * @Description: 添加商户通道
 * @Author: Joker
 * @Date: 2019.04.02 10:02
 * @Param: Merchant
 * @return: int: 判断成功的标识
 * @return: int64:	在表中哪一行插入
 **/
func (*Merchant) InsertMerchant(m Merchant) (int, int64) {
	om := orm.NewOrm()
	in, _ := om.QueryTable(MerchantTBName()).PrepareInsert()
	id, err := in.Insert(&m)
	if err != nil {
		sys.LogError("InsertMerchant failed to insert for: ", err)
		return utils.FAILED_FLAG, id
	}
	return utils.SUCCESS_FLAG, id
}

//判断商户编号是否存在
func (*Merchant) MerchantNoIsExit(no string) bool {
	om := orm.NewOrm()
	return om.QueryTable(MerchantTBName()).Filter("merchant_no", no).Exist()
}

/*查询分页*/
func (*Merchant) SelectMerchantListPage(params map[string]interface{}, limit, offset int) (list []Merchant) {
	om := orm.NewOrm()
	qt := om.QueryTable(MerchantTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	qt = qt.Filter("status", 0)
	_, err := qt.OrderBy("-edit_time").Limit(limit, offset).All(&list)
	if err != nil {
		sys.LogError("SelectMerchantListPage failed to query paging for: ", err)
	}
	return list
}

func (*Merchant) SelectMerchantPageCount(params map[string]interface{}) int {
	om := orm.NewOrm()
	qt := om.QueryTable(MerchantTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	qt = qt.Filter("status", 0)
	cnt, err := qt.Count()
	if err != nil {
		sys.LogError("SelectMerchantPageCount failed to count for:", err)
	}
	return int(cnt)
}

// 读取单个商户通道信息
func (*Merchant) SelectOneMerchant(id string) (m Merchant) {
	om := orm.NewOrm()
	_, err := om.QueryTable(MerchantTBName()).Filter("id", id).All(&m)
	if err != nil {
		sys.LogError("SelectOneMerchant failed to select one:", err)
	}
	return m
}

// 读取单个商户通道信息通过商户编号
func (*Merchant) SelectOneMerchantByNo(no string) (m Merchant) {
	om := orm.NewOrm()
	_, err := om.QueryTable(MerchantTBName()).Filter("merchant_no", no).All(&m)
	if err != nil {
		sys.LogError("SelectOneMerchantByNo failed to select one:", err)
	}
	return m
}

// 修改配置信息
func (*Merchant) UpdateMerchant(mt Merchant) int {
	om := orm.NewOrm()
	_, err := om.QueryTable(MerchantTBName()).Filter("id", mt.Id).Update(orm.Params{
		"version":       orm.ColValue(orm.ColAdd, 1),
		"merchant_name": mt.MerchantName,
		"merchant_no":   mt.MerchantNo,
		"secret_key":    mt.SecretKey,
		"edit_time":     mt.EditTime,
		"total_amount":  mt.TotalAmount,
		"usable_amount": mt.UsableAmount,
		"frozen_amount": mt.FrozenAmount,
		"status":        mt.Status,
	})
	if err != nil {
		sys.LogError("UpdateMerchant failed to update for:", err)
		return utils.FAILED_FLAG
	}
	return utils.SUCCESS_FLAG
}

//选择单通道下所有商户
func (*Merchant) SelectAllMerchantByType(t string) (list []Merchant) {
	om := orm.NewOrm()
	qt := om.QueryTable(MerchantTBName())
	qt = qt.Filter("channel_type", t)
	_, err := qt.OrderBy("-edit_time").All(&list)
	if err != nil {
		sys.LogError("SelectAllMerchantByType failed to query merchant for: ", err)
	}
	return list
}

// 根据条件查询所有激活商户
func (*Merchant) SelectAllMerchantBy(params map[string]interface{}) (list []Merchant) {
	om := orm.NewOrm()
	qt := om.QueryTable(MerchantTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	qt = qt.Filter("status", 0)
	_, err := qt.OrderBy("-edit_time").All(&list)
	if err != nil {
		sys.LogError("SelectAllMerchantBy failed to query paging for: ", err)
	}
	return list
}
